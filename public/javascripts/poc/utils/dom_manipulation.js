/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 5:42 PM
 * @file dom_manipulation.js
 * @desc Adds SmartTV JavaScript and CSS SDK to header.
 */

// 'use strict';

// cacheing jquery selectors
var $head = $('head');


/**
 * @public
 * @function addCss
 * @param {string} src Path / URL string of css file.
 * @memberof poc.Utils.Dom_manipulation
 */
poc.Utils.Dom_manipulation.addCss = function(src) {
  var rel = 'stylesheet',
      css = document.createElement('link');
  css.setAttribute('rel', rel);
  css.setAttribute('href', src);
  $head.append(css);
};


/**
 * @public
 * @function addScript
 * @param {string} src Path / URL string of javascript file.
 * @memberof poc.Utils.Dom_manipulation
 */
poc.Utils.Dom_manipulation.addScript = function(src) {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = src;
  script.setAttribute('language', 'javascript');
  $head.append(script);
};
