/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/3/13
 * Time: 5:31 PM
 * @file strings.js
 * @desc String utilities.
 */

// 'use strict';


/**
 * @public
 * @function uppercaseMe
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.uppercaseMe = function(txt) {
  var firstLetter = txt.charAt(0),
      remainingText = txt.substr(1);

  return firstLetter.toUpperCase() + remainingText.toLowerCase();
};


/**
 * @public
 * @function urlencode
 * @memberof poc.Utils.Strings
 * @param {string} str
 * @return {string}
 */
poc.Utils.Strings.urlencode = function(str) {
  // to get rid of chained call to
  // function code style errors
  var encode = encodeURIComponent(str),
      exclamation = encode.replace(/!/g, '%21'),
      singleQuote = exclamation.replace(/'/g, '%27'),
      leftParen = singleQuote.replace(/\(/g, '%28'),
      rightParen = leftParen.replace(/\)/g, '%29'),
      backSlash = rightParen.replace(/\*/g, '%2A');

  return backSlash.replace(/%20/g, '+');
};


/**
 * @public
 * @function shortenTitle
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.shortenTitle = function(txt) {
  return txt.substr(0, 18) + ' . . .';
};


/**
 * @public
 * @function shortenTitle
 * @memberof poc.Utils.Strings
 * @param {string} txt
 * @return {string}
 */
poc.Utils.Strings.shortenDesc = function(txt) {
  return txt.substr(0, 40) + ' . . .';
};
