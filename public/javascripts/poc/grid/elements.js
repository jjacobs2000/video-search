/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/4/13
 * Time: 4:32 PM
 * @file elements.js
 * @desc Elements dynamically created by api data for grid layout.
 */

// 'use strict';


/**
 * @public
 * @function objects
 * @memberof poc.Grid.Elements
 * @return {Array}
 */
poc.Grid.Elements.objects = [];


/**
 * @public
 * @function playing
 * @memberof poc.Grid.Elements
 * @return {Boolean}
 */
poc.Grid.Elements.playing = false;

// only used in browser dev tools console
poc.Grid.Elements.debugData = function () {
    if ($("#video_container").length > 0) {
           return {"window height" : window.innerHeight,
            "window width" : window.innerWidth,
            "camera z pos" : Math.abs(
                parseInt(
                    $("#video_container")
                        .children("div")
                        .eq(0)
                        .children()[0]
                        .attributes.style.nodeValue
                        .split(': ')[4]
                        .split(') ')[1]
                        .split(', ')[14]
                )
            )
        };
    };
};

/**
 * @public
 * @function elements
 * @memberof poc.Grid.Elements
 * @param {Object} data Data from provider APIs.
 */
poc.Grid.Elements.elements = function (data) {

    _.each(data[0], function (item, i) {

        // poster image for video element
        var elPoster = $('<img/>', {
            id : 'element_poster_' + i,
            src : item.thumbnail
        })
        .addClass('elposter smaller');

        // details
        var info = $('<p/>')
        .append(
            $('<img/>', {
                src : item.icon,
                align: 'center'
            })
            .addClass('element_icon'),
            $('<span/>', {
                html: poc.Utils.Strings.shortenTitle(item.title)
            })
            .addClass('greenInfo')
        )
        .addClass('elementspaceygreen');

        var element_darker_overlay = $('<div/>')
          .addClass('darker_overlay')
            .css({
              'background-color': 'black',
              opacity: '0.6',
              position: 'absolute',
              cursor: 'pointer',
              height: '471px',
              width: '480px',
              'z-index': 999
            });

        // parent element
        var element = $('<div/>', {
            id: 'element_id_' + i
        })
        .addClass('video_element smaller')
        .append(elPoster, info);

        var element_container = $('<div/>')
            .addClass('smaller')
            .append(element_darker_overlay, element);

        $(element_container).on('mouseleave', function(){
            $(element_darker_overlay).css({
                opacity: '0.6'
            });
        });

        $(element_container).on('mouseenter', function(){
            $(element_darker_overlay).css({
                opacity: '0'
            });
        });

        // resize everything when browser window size changes
        $(window).resize(function () {
            poc.Grid.Layout.sceneElements.camera.aspect = $(window).width() / $(window).height();
            poc.Grid.Layout.sceneElements.camera.updateProjectionMatrix();

            poc.Grid.Layout.sceneElements.renderer.setSize($(window).width(), $(window).height());
        });

        var object = new THREE.CSS3DObject($(element_container)[0]);

        poc.Grid.Elements.objects.push(object);

        element_container.properties = { embedly_uri : item.embedly_uri };

        // Create embedly populated video detail panel
        $(element_container).on("click", function () {
            $(".icon-search").hide();
            $(".icon-home").hide();
            $(".icon-remove-sign").show();
            var item = poc.Data.Api.Embedly.data(element_container.properties.embedly_uri);
            poc.Grid.Details.panel(item);
        });

    });
};
