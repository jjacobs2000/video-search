/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/4/13
 * Time: 3:23 PM
 * @file layout.js
 * @desc Layout for video search result posters CSS3D grid.
 */

// 'use strict';


/**
 * @public
 * @function runGrid
 * @memberof poc.Grid.Layout
 * @desc Client method to
 * create the video element grid layout.
 */
poc.Grid.Layout.runGrid = function() {
  poc.Grid.Layout.init();
  poc.Grid.Layout.animate();
};


/** @global */
poc.Grid.Layout.orgObjectsPosition = [];


/** @global */
poc.Grid.Layout.sceneElements = {};


/** @global */
poc.Grid.Layout.totalObj = 50;


/**
 * @public
 * @function init
 * @memberof poc.Grid.Layout
 * @desc Defines scene elements for layout.
 */
poc.Grid.Layout.init = function() {
    var camera, scene, renderer, controls;

    var fov = 75;
    var aspect = window.innerWidth / window.innerHeight;
    var near = 0.1;
    var far = 5000;

    camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

    // camera is center & 'front' or screen
    camera.position.z = poc.Data.Layout.Presets.settings().cameraZ;    // lower the number 'closer' to objects
    camera.position.x = poc.Data.Layout.Presets.settings().cameraX;  // lower the number more to the right
    camera.position.y = poc.Data.Layout.Presets.settings().cameraY; // lower the number the more up

    scene = new THREE.Scene();

    // CSS3D renderer
    renderer = new THREE.CSS3DRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.domElement.style.position = 'absolute';
    document.getElementById('video_container').appendChild(renderer.domElement);

    // TrackballControls also handles gestures
    controls = new THREE.TrackballControls( camera );

    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;

    controls.noZoom = false;
    controls.noPan = false;

    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    controls.keys = [ 65, 83, 68 ];

    controls.addEventListener('change', poc.Grid.Layout.render);

    poc.Grid.Layout.sceneElements = {
    camera: camera,
    scene: scene,
    renderer: renderer,
    controls: controls
  };

};


/**
 * @public
 * @function render
 * @memberof poc.Grid.Layout
 * @desc Renders scene.
 */
poc.Grid.Layout.render = function () {
    poc.Grid.Layout.sceneElements.renderer.render(poc.Grid.Layout.sceneElements.scene, poc.Grid.Layout.sceneElements.camera);
};


/**
 * @public
 * @function animate
 * @memberof poc.Grid.Layout
 * @desc Animates scene.
 */
poc.Grid.Layout.animate = function () {
    requestAnimationFrame(poc.Grid.Layout.animate);
    TWEEN.update();
    poc.Grid.Layout.sceneElements.controls.update();
};


/**
 * @public
 * @function createGrid
 * @memberof poc.Grid.Layout
 * @param {Object} data Video provider data.
 * @this createGrid
 * @desc Creates grid layout.
 */
poc.Grid.Layout.createGrid = function (data) {
    var grid = [];
    poc.Grid.Elements.elements(data);
    var dynPos = poc.Data.Layout.Presets.settings();

    var totalObj =  (Math.floor(poc.Grid.Layout.totalObj / (dynPos.objDown * dynPos.objAcross)) * (dynPos.objDown * dynPos.objAcross));

    var objects = poc.Grid.Elements.objects;

    _.each(poc.Grid.Elements.objects, function (object, i) {
        if (i < totalObj) {
            var delay = Math.random() * 1000;

            new TWEEN.Tween(object.position)
                .to({ y: -3000 }, 1000)
                .delay(delay)
                .easing(TWEEN.Easing.Exponential.In)
                .start();

            new TWEEN.Tween(object)
                .to({}, 2000)
                .delay(delay)
                .onComplete(function () {

                    poc.Grid.Layout.sceneElements.scene.remove(this);
                    poc.Grid.Layout.sceneElements.cameraElement.removeChild(this.element);

                    var index = this.objects.indexOf(this);
                    this.objects.splice(index, 1);
                })
                .start();
        }
    });

    _.each(poc.Grid.Elements.objects, function (object, i) {

        if (i < totalObj) {
            object.position.x = Math.random() * 4000 - 2000;
            object.position.y = Math.random() * 4000 - 2000;
            object.position.z = Math.random() * 4000 - 2000;

            poc.Grid.Layout.sceneElements.scene.add(object);

            var object = new THREE.Object3D();

            object.position.x = ( ( i % dynPos.objAcross ) * dynPos.spaceBtwObj ) - ((Math.round(dynPos.objAcross / 2) - 1) * dynPos.spaceBtwObj);
            object.position.y = ( -( Math.floor(i / dynPos.objAcross) % dynPos.objDown ) * dynPos.spaceBtwObj ) + ((Math.round(dynPos.objDown / 2) - 1) * dynPos.spaceBtwObj);
            object.position.z = ( Math.floor(i / (dynPos.objDown * dynPos.objAcross)) ) * dynPos.spaceBtwLayers;

            grid.push(object);
        }
    });



    var duration = 2000;

    TWEEN.removeAll();

    _.each(poc.Grid.Elements.objects, function (object, i) {
        if (i < totalObj) {
            var object = objects[ i ];
            var target = grid[ i ];

            new TWEEN.Tween(object.position)
                .to({ x: target.position.x, y: target.position.y, z: target.position.z }, Math.random() * duration + duration)
                .easing(TWEEN.Easing.Exponential.InOut)
                .start();

            new TWEEN.Tween(object.rotation)
                .to({ x: target.rotation.x, y: target.rotation.y, z: target.rotation.z }, Math.random() * duration + duration)
                .easing(TWEEN.Easing.Exponential.InOut)
                .start();

            poc.Grid.Layout.orgObjectsPosition.push(object.position);
        }
    });

    new TWEEN.Tween(this)
        .to({}, duration * 2)
        .onUpdate(poc.Grid.Layout.render)
        .start();

};