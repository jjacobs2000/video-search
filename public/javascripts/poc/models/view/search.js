/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/4/13
 * Time: 12:05 PM
 * @file search.js
 * @desc Knockout.js Search view model.
 */

// 'use strict';


/**
 * @public
 * @function start
 * @memberof poc.Models.View.Search
 */
poc.Models.View.Search.start = function () {
  var IMAGE_CHECKED = '../img/smallCheckMark.png',
    IMAGE_WIDTH = '33px',
    IMAGE_HEIGHT = '33px',
    IMAGE_CLASS = 'white rounded-corners shadow',
    IMAGE_PADDING = '4px 0 0 3px',

  // cacheing jquery selectors
    $homeButton = $('.icon-home.span3.text-right'),
    $search = $('#search'),
    $providers = $('#providers'),
    $query = $('#query'),
    $location = $(location),
    $locationHref = $location.attr('href'),
    $window = $(window),
    $newDiv = $('<div />'),
    $newInput = $('<input/>'),
    $newImage = $('<img />'),
    $search_box = $('#search_box'),
    $search_button = $('#search_button'),

    query = $.totalStorage('query'),

  // Check boxes for providers
    providers = [],
    hrefArray = $locationHref.split('/');

  /**
   * @private
   * @function viewModel
   * @todo incorporate video provider data into view model
   */
  var viewModel = function () {
  };


  /**
   * @public
   * @function wireEvents
   * @desc jQuery events for search.
   */
  var wireEvents = function () {
    $homeButton.on('click', function(){
      window.location.href = "#/menu";
    });
    var createProviderElements;
    var i,
      providerServiceApi = poc.Data.Api.Embedly.providerServiceApi;

    $search.css({
      height: $window.height(),
      width: $window.width()
    });

    /**
     * @private
     * @function createProviderElement
     * @desc Creates provider search check buttons
     * and sends query to data retrieval methods
     */
    createProviderElements = function (provider, i) {
      // to get rid of chained call to
      // function code style errors
      var $newInputId = $newInput.attr('id', provider.name),
        $newInputType = $newInputId.attr('type', 'checkbox');
      var container = $('<div></div>')
        .attr('id', provider.name + "_id")
        .css('float', 'left')
        .addClass('white rounded-corners shadow');

      if (provider.enabled) {
        var inputElem = $('<input/>')
          .attr('id', provider.name)
          .attr('type', 'checkbox')
          .attr('tabindex', i);

        container.append(inputElem);

        inputElem.overlayImageCheck({
          image: provider.favicon,
          imageChecked: IMAGE_CHECKED,
          overlayCheckedImage: true,
          width: IMAGE_WIDTH,
          height: IMAGE_HEIGHT,
          wasChecked: $.totalStorage(provider.name) ? true : false,
          afterCheck: function (isChecked) {
            if (isChecked) {
              //save provider name and object in localstorage
              $.totalStorage(
                provider.name,
                {
                  name: provider.name,
                  icon: provider.favicon
                }
              );
            } else {
              localStorage.clear();
            }
          }
        });
      } else {
        var imageWrapper = $('<div />').css({
          cursor: 'pointer',
          position: 'relative',
          width: IMAGE_WIDTH,
          height: IMAGE_HEIGHT,
          padding: IMAGE_PADDING
        });
        var disabledImageElem = $('<img />')
          .attr('src', provider.favicon)
          .css({
            width: IMAGE_WIDTH,
            height: IMAGE_HEIGHT
          });
        container.addClass('disabled');
        container.append(imageWrapper);
        imageWrapper.append(disabledImageElem);
      }

      $providers.append(container);
    };

    _.each(providerServiceApi()[0], function (provider, index) {
      createProviderElements(provider, index);
    });

    // set the query value to an empty string so
    // when the page first loads, the query
    // value is an empty string
    if (typeof hrefArray[6] == 'undefined') {
      $query.val('');
      $.totalStorage.deleteItem('query');
    } else {
      $search_box.hide();
      var query = hrefArray[6];
      $query.val(query);
      // when refreshing the page, get providers from localstorage
      if (localStorage.length > 0) {
        for (i = 0; i < localStorage.length; i++) {
          if (localStorage.getItem(localStorage.key(i)) !== null) {
            providers.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
          }
        }
      }
      poc.Grid.Layout.runGrid();
      poc.Data.Api.Data.createDataObj(query, providers);
    }

    // Events to get param
    $query.on('keyup', this, function (event) {
      if (event.keyCode === 13) {
        $search_box.fadeOut({
          done: function () {
            // add query param to url
            if ($query.val() != '') {
              window.location.hash = '/search/' + $query.val();
            }
          }
        });
      }
    });

    // execute search function when search button is pressed
    $search_button.on('click', function () {
      if ($query.val() != '') {
        window.location.hash = '/search/' + $query.val();
      }
    });
  };

  wireEvents();
  ko.applyBindings(viewModel, document.getElementById('search'));

};
