/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 1:14 PM
 * @file buttons.js
 * @desc Data to create dynamic main menu buttons.
 */

// 'use strict';


/**
 * @public
 * @function menu
 * @memberof poc.Data.Layout.Buttons
 * @return {Object}
 */
poc.Data.Layout.Buttons.menu = {
  'buttons': [
    {
      'style': 'icon-search',
      'button_title': 'Search',
      'view': 'search'
    },
    {
      'style': 'icon-th',
      'button_title': 'Library',
      'view': 'library'
    },
    {
      'style': 'icon-comments-alt',
      'button_title': 'Social',
      'view': 'social'
    },
    {
      'style': 'icon-cog',
      'button_title': 'Settings',
      'view': 'settings'
    }
  ]
};
