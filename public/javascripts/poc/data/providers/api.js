/**
 * @author julia_jacobs@labs.att.com (Julia Jacobs)
 * Date: 6/6/13
 * Time: 12:42 PM
 * @file api.js
 * @desc Hard coded values for different providers needed for API requests.
 */

// 'use strict';


/**
 * @public
 * @function getProviderData
 * @memberof poc.Data.Providers.Api
 * @param {String} providerName Video provider name.
 * @return {Object}
 */
poc.Data.Providers.Api.getProviderData = function(providerName) {
  var type = 'json',
      providerInfo = {
            providers: [
          {
            name: 'youtube',
            url: function(query) {
              return 'https://gdata.youtube.com/feeds/api/videos?' +
                  'v=2' +
                  '&alt=' +
                  type +
                  'c' +
                  '&safeSearch=moderate' +
                  '&start-index=1' +
                  '&max-results=50' +
                  '&hd=true' +
                  '&key=AIzaSyCJ-YABHvUb--o4iofdUu9O8wW5uomRDzc' +
                  '&q=' +
                  query;
            },
            items: function(data) {
              return data.data.items;
            },
            thumbnail: function(item) {
              return item.thumbnail.hqDefault;
            },
            embedly_uri: function(item) {
              return 'http://www.youtube.com/v/' + item.id;
            },
            title: function(item) {
              return item.title;
            },
            description: function(item) {
              return item.description;
            }
          }
            ]
      };

  var selectedProvider = [];

  _.each(providerInfo.providers, function(provider) {
    if (provider.name === providerName) {
      selectedProvider.push(provider);
    }
  });

  return selectedProvider[0];

};
