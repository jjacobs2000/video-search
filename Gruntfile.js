module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-shell');

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/* <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    clean: {
      files: ['dist']
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true,
        separator: ';'
      },
      dist: {
        src: ['public/javascripts/poc/namespace.js',
          'public/javascripts/libs/underscore.js',
          'public/javascripts/libs/jquery-1.9.1.js',
          'public/javascripts/libs/jquery-ui-1.10.3.custom.js',
          'public/javascripts/libs/jquery-migrate-1.1.1.js',
          'public/javascripts/libs/jquery.throttledresize.js',
          'public/javascripts/libs/modernizr-latest.js',
          'public/javascripts/libs/detectizr.js',
          'public/javascripts/libs/screenfull.js',
          'public/javascripts/poc/utils/detect.js',
          'public/javascripts/poc/utils/transformLogo.js',
          'public/javascripts/libs/jquery.total-storage.js',
          'public/javascripts/libs/jquery-social-share-buttons/js/jquery.social.share.2.2.js',
          'public/javascripts/libs/jquery.prettify.js',
          'public/javascripts/poc/utils/strings.js',
          'public/javascripts/libs/knockout-2.2.0.debug.js',
          'public/javascripts/libs/knockout.mapping-latest.debug.js',
          'public/javascripts/libs/infuser.js',
          'public/javascripts/libs/TrafficCop.js',
          'public/javascripts/libs/koExternalTemplateEngine_all.js',
          'public/javascripts/libs/sammy/lib/sammy.js',
          'public/javascripts/libs/sammy/lib/plugins/sammy.mustache.js',
          'public/javascripts/libs/sammy/lib/plugins/sammy.json.js',
          'public/javascripts/libs/jquery.overlayImageCheck.js',
          'public/javascripts/libs/jquery.transit.js',
          'public/javascripts/poc/data/providers/names.js',
          'public/javascripts/poc/data/providers/api.js',
          'public/javascripts/poc/data/api/embedly.js',
          'public/javascripts/poc/data/api/data.js',
          'public/javascripts/libs/three.js',
          'public/javascripts/libs/tween.js',
          'public/javascripts/libs/TrackballControls.js',
          'public/javascripts/libs/CSS3DRenderer.js',
          'public/javascripts/poc/router.js',
          'public/javascripts/poc/data/layout/presets.js',
          'public/javascripts/poc/grid/layout.js',
          'public/javascripts/poc/grid/details.js',
          'public/javascripts/poc/grid/elements.js',
          'public/javascripts/poc/data/layout/buttons.js',
          'public/javascripts/poc/models/view/menu.js',
          'public/javascripts/poc/models/view/search.js'
        ],
        dest: 'public/javascripts/build/poc.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        files: {
          'public/javascripts/build/poc.min.js': [
            'public/javascripts/build/poc.js'
          ]
        }
      }
    },
    watch: {
      scripts: {
        files: ['<%= concat.dist.src %>'],
        tasks: ['concat', 'uglify']
      },
      development: {
        files: ['<%= less.development.files %>'],
        tasks: ['less']
      },
      production: {
        files: ['<%= less.production.files %>'],
        tasks: ['less']
      },
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      src: {
        files: '<%= jshint.src.src %>',
        tasks: ['jshint:src', 'qunit']
      },
      test: {
        files: '<%= jshint.test.src %>',
        tasks: ['jshint:test', 'qunit']
      }
    },
    less: {
      development: {
        files: {
          'public/bootstrap/css/bootstrap.css': 'public/bootstrap/less/bootstrap.less',
          'public/bootstrap/css/responsive.css': 'public/bootstrap/less/responsive.less',
          'public/css/layout.css': 'public/css/layout.less',
          'public/css/base.css': 'public/css/base.less',
          'public/css/menu.css': 'public/css/menu.less',
          'public/css/imagechecks.css': 'public/css/imagechecks.less',
          'public/css/search.css': 'public/css/search.less',
          'public/css/social.css': 'public/css/social.less'
        }
      },
      production: {
        files: {
          'public/css/site.css': 'public/css/site.less'
        }
      }
    },
    shell: {
      docs: {
        command: './docs.sh'
      }
    }
  });

  // These plugins provide necessary tasks.

  // Default task.
  grunt.registerTask('default', ['jshint', 'qunit', 'clean', 'concat', 'uglify', 'less']);
  grunt.registerTask('build', ['clean', 'concat', 'uglify', 'less', 'shell']);

};
