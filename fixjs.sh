#!/bin/bash
########################################################
## Fixes closure linter                               ##
## (https://developers.google.com/closure/utilities/) ##
## errors in JavaScript files.  Requires closure      ##
## compiler                                           ##
## (https://developers.google.com/closure/compiler/)  ##
########################################################

for file in $(find $1 -name "*.js");
do
echo $file
fixjsstyle --strict --jsdoc --custom_jsdoc_tags=file,namespace,memberof,function,inner,public,private,protected,todo $file
done

